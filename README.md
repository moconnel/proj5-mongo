# Identifying information
Name: Michael O'Connell

Project: Project 5: Brevet time calculator with Ajax and MongoDB

Class: CIS 322: Introduction to Software Engineering, Spring 2020 at University of Oregon

Instructor: Professor Ram Durairajan

Date: 5/22/2020

# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## Brevet control time description and specification of control time rules

Controls are points where a rider must obtain a proof of passage, and control times are the minimum and maximum times by which the rider must arrive at the location.

Algorithm for calculating control times:
https://rusa.org/pages/acp-brevet-control-times-calculator

Calculator:
https://rusa.org/octime_acp.html

Additional rules:
https://rusa.org/pages/rulesForRiders

** Case 1: Slightly more than 200 km or less **

Example:
See table

Solution:
The calculation of a control's opening time is based on the max speed (for a given central location- see table) and closing time is based on the min speed. This is the case because max/min speed will be constant for all control points (all in the same column).

Calculation:
hrs = (location // speed (km/hr))
mins = (hrs - (location / speed)) * 60

Note:
Round the mins. Alternatively, you could also use modulus for minutes (location % speed) * 60

** Case 2: Finish is slightly above location threshold with 200 km official rule **

Example:
0 - 200km is a control location window with a given max and min speed and finish occurs at 205 km.

Solution:
(1) Use 200km in the calculation, even though the route was slightly longer than that AND (2) use the rule (which superceeds all other considerations) that 200 km brevet = 13 hr 30 min as an overall time limit. Even though 200 / 15 = 13 hr 20 min

Calculation:
if brevet is slightly over 200 then 13 hr 30 min is overall time limit

** Case 3: Brevet larger than 200 km with control points at distances also than 200 km**

Example:
600 km brevet with intermediate controls every 50 km with overall distance of 609 km.

Solution:
We'll use 3 rows of the table. First row of speeds for controls between 0 km and 200 km, second row of speeds for controls between 200 km and 400 km, and third row of speeds for controls between 400 km and 600 km.

Calculation(s):
A control at 100 km or 200 km, we'll simply use the table's first row.

Controls beyond 200 km, max speed decreases.

Consider control at 350 km:
200 / max speed in row 1 + 150 / max speed in row 2

Note:
Min speed in the above case is the same for the 2 rows, so you can simply divide by that number (350 km / min speed (the same for both rows)).

** Case 4: Control close to 0 **

Example:
By rule, starting point control is 1 hr after official start. If the organizer places a control within the first 15 km, the control will close before the starting point closes.

We're going to be using the French variation of the algorithm which helps mitigate this issue.

Solution:
The maximum time limit for a control within the first 60 km is based on 20 km/hr + 1 hr. The minimum is still the same (dist / speed for a given row).

Calculation:
10 km -> 10 / 20 + 1 hr
20 km -> 20 / 20 + 1 hr
...

Note:
This is the solution the web calculator opts for. Also, by 60 km, this yields the same result as the normal calculation, so this only needs to apply to < 60 km.

** Case 5: Overall time limit for brevets according to official rules **

Solution:
13:30 for 200 km
20:00 for 300 km
27:00 for 400 km
40:00 for 600 km
75:00 for 1000 km

Note:
These will have to be hardcoded into your algorithm.

-----------------------------------------------------------------------------
III. Calculator analysis with test cases
-----------------------------------------------------------------------------
** Case 1: Brevet overall time is unambigious (specified by rules) and control is close to 0 km **

Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    15    |    8:26   |  9:45
--------------------------------------------------
3	       |    56    |    9:39   |  11:48
--------------------------------------------------
4	       |    60    |    9:46   |  12:00
--------------------------------------------------
5              |    200   |    13:53  |  21:30
--------------------------------------------------

Calculations:
1.
None

2.
open: 15 / 34 = 26 min (round down)
close: 15 / 20 = 1 hr + 45 min

3.
open: 56 / 34 = 1 hr 39 min (round up)
close: 56 / 20 + 1 hr = 3 hr 48 min

4.
open: 60 / 34 = 1 hr 46 min (round up)
close: 60 / 15 = 3 hr

5.
open: 200 / 34 = 5 hr 53 min (round up)
close: 13 hr 30 min (official rules)

** Case 2: Overall brevet time is ambigious **

Example 1:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    220   |    13:53  |  21:30
--------------------------------------------------

Example 2:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    240   |    13:53  |  21:30
--------------------------------------------------

Example 3:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    241   |    **     |  **
--------------------------------------------------

Example 4:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    270   |    **     |  **
--------------------------------------------------
** Error: the last control point (270.00 km) is over 20% longer than the theoretical distance (200 km): error in selecting units?

Note: 20% of 200 is 40.

** Case 3: Overall brevet time is dictated by rules **

Example 1:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    200   |    13:53  |  21:30
--------------------------------------------------

Example 2:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    300   |    17:00  |  04:00
--------------------------------------------------

Example 3:
Checkpoint No. | Distance | Open time | Close time | Day
---------------------------------------------------------
1	       |    0     |    8:00   |  9:00      | 0
---------------------------------------------------------
2	       |    400   |    20:08  |  11:00     | 1
---------------------------------------------------------

Example 4:
Checkpoint No. | Distance | Open time | Close time | Day
----------------------------------------------------------
1	       |    0     |    8:00   |  9:00      | 0
----------------------------------------------------------
2	       |    600   |    02:48  |  00:00     | 1 (open) & 2 (close)
----------------------------------------------------------

Example 5:
Checkpoint No. | Distance | Open time | Close time | Day
----------------------------------------------------------
1	       |    0     |    8:00   |  9:00      | 0
----------------------------------------------------------
2	       |    1000  |    17:05  |  11:00     | 1 (open) & 3 (close)
----------------------------------------------------------

** Case 4: Normal case: 200 km brevet with controls at 60 km, 120 km, 175 km and finish (205 km) **

Example 2:
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    60    |    09:46  |  12:00
--------------------------------------------------
3	       |    120   |    11:32  |  16:00
--------------------------------------------------
4	       |    175   |    13:09  |  19:40
--------------------------------------------------
5	       |    205   |    13:53  |  21:30
--------------------------------------------------

** Case 5: Over 200 km brevet: 600 km brevet with controls at every 50 km and overall distance of 609 km**

600km ACP BREVET
Checkpoint       Date  Time
==========       ====  ====
    0km   start: 13/05 08:00
          close: 13/05 09:00

   50km    open: 13/05 09:28
          close: 13/05 11:30

  100km    open: 13/05 10:56
          close: 13/05 14:40

  150km    open: 13/05 12:25
          close: 13/05 18:00

  200km    open: 13/05 13:53
          close: 13/05 21:20

  250km    open: 13/05 15:27
          close: 14/05 00:40

  300km    open: 13/05 17:00
          close: 14/05 04:00

  350km    open: 13/05 18:34
          close: 14/05 07:20

  400km    open: 13/05 20:08
          close: 14/05 10:40

  450km    open: 13/05 21:48
          close: 14/05 14:00

  500km    open: 13/05 23:28
          close: 14/05 17:20

  550km    open: 14/05 01:08
          close: 14/05 20:40

  600km    open: 14/05 02:48
          close: 15/05 00:00

## Description of functionality added
Submit and display buttons have been added to the system.
'Submit' sends form data (km, brevet km distance, open times, etc.) to Flask to be modified and added to a MongoDB database.
'Display' retrieves database data and re-formats to be successfully displayed in 'todo.html'

'todo.html' and 'empty.html' have both been added for displaying data and giving appropriate error messages.

## Button test cases
Submit:
(1) No data entered into form will route to an error page with information about the error and a link back to the index page.

(2)
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    60    |    09:46  |  12:00
--------------------------------------------------
3	       |    120   |    11:32  |  16:00
--------------------------------------------------
4	       |    175   |    13:09  |  19:40
--------------------------------------------------
5	       |    205   |    13:53  |  21:30
--------------------------------------------------
Submitted successfully.

(3) 600 km brevet with controls at every 50 km and overall distance of 609 km submitted successfully


Display:
(1) No data entered into form will route to an error page with information about the error and a link back to the index page.

(2)
Checkpoint No. | Distance | Open time | Close time
--------------------------------------------------
1	       |    0     |    8:00   |  9:00
--------------------------------------------------
2	       |    60    |    09:46  |  12:00
--------------------------------------------------
3	       |    120   |    11:32  |  16:00
--------------------------------------------------
4	       |    175   |    13:09  |  19:40
--------------------------------------------------
5	       |    205   |    13:53  |  21:30
--------------------------------------------------
Successfully displayed.

(3) 600 km brevet with controls at every 50 km and overall distance of 609 km displayed successfully
600km ACP BREVET
Checkpoint       Date  Time
==========       ====  ====
    0km   start: 13/05 08:00
          close: 13/05 09:00

   50km    open: 13/05 09:28
          close: 13/05 11:30

  100km    open: 13/05 10:56
          close: 13/05 14:40

  150km    open: 13/05 12:25
          close: 13/05 18:00

  200km    open: 13/05 13:53
          close: 13/05 21:20

  250km    open: 13/05 15:27
          close: 14/05 00:40

  300km    open: 13/05 17:00
          close: 14/05 04:00

  350km    open: 13/05 18:34
          close: 14/05 07:20

  400km    open: 13/05 20:08
          close: 14/05 10:40

  450km    open: 13/05 21:48
          close: 14/05 14:00

  500km    open: 13/05 23:28
          close: 14/05 17:20

  550km    open: 14/05 01:08
          close: 14/05 20:40

  600km    open: 14/05 02:48
          close: 15/05 00:00
